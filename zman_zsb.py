#!/usr/bin/python
#
# zman_zsb.py - Automate ZENworks Backup Command
#
# Author: Maer Melo
# 	  mmelo[at]novell[dot]com
# Date: Oct 29, 2012

import sys, os

def usage():
	print 'Usage: zman_zsb.py <Backup file path> <Passphrase>'
	return

if (len(sys.argv) != 3):
	usage()
	sys.exit(2)

backup_file = sys.argv[1]
passphrase = sys.argv[2]
lf = chr(10)
f = os.popen("zman zsb " + backup_file, 'w')
f.write(passphrase + lf)
sys.exit(0)
